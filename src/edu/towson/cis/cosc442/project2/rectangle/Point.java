package edu.towson.cis.cosc442.project2.rectangle;

/**
 * The Point Class.
 */
public class Point {
	
	/** x and y coordinates. */
	public Double x, y;
	
	/**
	 * Instantiates a new point.
	 *
	 * @param x the x
	 * @param y the y
	 */
	Point(Double x, Double y) {
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Gets the x point.
	 *
	 * @return the x value
	 */
	public Double getX() {
		return this.x;
	}
	
	/**
	 * Gets the y point.
	 *
	 * @return the y value
	 */
	public Double getY() {
		return this.y;
	}
}
