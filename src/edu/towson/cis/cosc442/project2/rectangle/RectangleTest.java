package edu.towson.cis.cosc442.project2.rectangle;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The unit test Class for Rectangle.
 */
public class RectangleTest {
	
	/** Declaring necessary test objects for {@link Rectangle} */
	Rectangle rect1, rect2;
	Point point1, point2;
	Point rect1Point1, rect1Point2, rect2Point1, rect2Point2;

	/**
	 * Initializes the necessary test objects for the test cases to use.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		
		rect1Point1 = new Point(2.0, 2.0);
		rect1Point2 = new Point(4.0, 7.0);
		rect2Point1 = new Point(2.0, 6.0);
		rect2Point2 = new Point(4.0, 3.0);
		
		//previous code:
		//rect1 = new Rectangle(new Point(2.0, 2.0), new Point(4.0, 7.0));
		//rect2 = new Rectangle(new Point(2.0, 6.0), new Point(4.0, 3.0));
		rect1 = new Rectangle(rect1Point1, rect1Point2); //extracted points into variables, see above,
		rect2 = new Rectangle(rect2Point1, rect2Point2); //since they're reused again later @testRectangle~~~~Point
		
		point1 = new Point (1.0, 2.0);
		point2 = new Point (3.0, 5.0);
	}

	/**
	 * Test for the getArea() method of the {@link Rectangle} class.
	 */
	@Test
	public void testGetArea() {
		assertEquals(10.0, rect1.getArea(), 0.001);
		assertEquals(6.0, rect2.getArea(), 0.001);
	}

	/**
	 * Test for the getDiagonal() method of the {@link Rectangle} class.
	 */
	@Test
	public void testGetDiagonal() {
		assertEquals(5.3852, rect1.getDiagonal(), 0.0001);
		assertEquals(3.6056, rect2.getDiagonal(), 0.0001);
	}
	
	/**
	 * Test for the getX() method of {@link Point} class.
	 */
	@Test
	public void testPointX() {
		assertEquals(1.0, point1.getX(), 0.0001);
		assertEquals(3.0, point2.getX(), 0.0001);
	}
	
	/**
	 * Test for the getY() method of {@link Point} class.
	 */
	@Test
	public void testPointY() {
		assertEquals(2.0, point1.getY(), 0.0001);
		assertEquals(5.0, point2.getY(), 0.0001);
	}
	
	/**
	 * Test for the getFirstPoint() of the {@link Rectangle} class.
	 */
	@Test
	public void testRectangleFirstPoint() {
		assertEquals(rect1Point1, rect1.getFirstPoint());
		assertEquals(rect2Point1, rect2.getFirstPoint());
	}
	
	/**
	 * Test for the getSecondPoint of the {@link Rectangle} class.
	 */
	@Test
	public void testRectangleSecondPoint() {
		assertEquals(rect1Point2, rect1.getSecondPoint());
		assertEquals(rect2Point2, rect2.getSecondPoint());
	}
	
	/**
	 * Cleans up test objects after a test case is executed.
	 */
	@After
	public void tearDown(){
		rect1 = null;
		rect2 = null;
		
		point1 = null;
		point2 = null;
		
		rect1Point1 = null;
		rect1Point2 = null;
		rect2Point1 = null;
		rect2Point2 = null;
		
	}
}
